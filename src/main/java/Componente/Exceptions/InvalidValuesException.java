/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componente.Exceptions;

/**
 *
 * @author loliv
 */
public class InvalidValuesException extends RuntimeException {

    public InvalidValuesException() {
    }

    public InvalidValuesException(String message) {
        super(message);
    }

    public InvalidValuesException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidValuesException(Throwable cause) {
        super(cause);
    }

    public InvalidValuesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
    
    
}
