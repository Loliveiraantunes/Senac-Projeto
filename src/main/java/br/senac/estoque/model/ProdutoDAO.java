/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.estoque.model;

import Componente.Conexao;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.Produto;
import Componente.interfaces.BaseDAO;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.GrupoProduto;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.Mercadoria;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.TipoProduto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author loliv
 */
public class ProdutoDAO implements BaseDAO<Produto, Long>{

       private  Connection conn = Conexao.getInstance().getConnection();
    
    public static void main(String[] args){
        ProdutoDAO dao = new ProdutoDAO();
        Produto produto = new Mercadoria();
        GrupoProduto grupoProd = new GrupoProduto();
        grupoProd.setIdGrupoProduto(1);
        
        produto.setNomeProduto("Massa");
        produto.setDataCriacao(new Date());
        produto.setPerciCMS(10.5F);
        produto.setTipoProduto(TipoProduto.MERCADORIA);
        produto.setGrupoProduto(grupoProd);
        Long idProduto = dao.inserir(produto);
        System.out.println(idProduto);
        
        produto.setIdProduto(idProduto);
        produto.setDataAlteracao(new Date());
        produto.setNomeProduto("Massa 1");
        dao.alterar(produto);
    }
    
    @Override
    public Produto getPorId(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*Statement*/
    @Override
    public Long inserir(Produto produto){
        
           try {
               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               String sql = "INSERT INTO `projeto`.`produto`\n" +
                       "(`nomeProduto`,`tipo`,`percICMS`,\n" +
                       "`dataCriacao`,\n" +
                       "`idgrupoproduto`)\n" +
                       "VALUES\n" +
                       "('"+ produto.getNomeProduto() +"',"
                       + ""+ produto.getTipoProduto().getId()  +","
                       + ""+ produto.getPerciCMS()+","
                       + "{ts '"+ sdf.format(produto.getDataCriacao())   +"'},";
               if(produto.getGrupoProduto() == null)
                   sql += " null)";
               else
                   sql += " "+ produto.getGrupoProduto().getIdGrupoProduto()  +")";
               System.out.println(sql);
               
               
               Statement stm = conn.createStatement();
               int regInseridos = stm.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
               
               ResultSet rsChaveGerada = stm.getGeneratedKeys();
               rsChaveGerada.next(); //Primeira linha
               return rsChaveGerada.getLong(1);
           } catch (SQLException ex) {
               throw  new RuntimeException("Não foi possível Inserir este Produto");
           }
    }

    @Override
    public boolean excluir(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean alterar(Produto produto) {
        
           try {
               String sql = "UPDATE `projeto`.`produto`\n" +
                       "SET\n" +
                       "`nomeProduto` = ?,\n" +
                       "`tipo` = ?,\n" +
                       "`percICMS` = ?,\n" +
                       "`dataAlteracao` = ?,\n" +
                       "`idgrupoproduto` = ?\n" +
                       "WHERE `idProduto` = ?";
               
               PreparedStatement ps = conn.prepareStatement(sql);
               ps.setString(1, produto.getNomeProduto());
               ps.setObject(2, produto.getTipoProduto().getId()); //int
               ps.setFloat(3, produto.getPerciCMS()); //float
               ps.setTimestamp(4, new java.sql.Timestamp(produto.getDataAlteracao().getTime()));
               //ps.setDate(4, new java.sql.Date(produto.getDataAlteracao().getTime()));
               if(produto.getGrupoProduto() == null)
                   ps.setObject(5, null);
               else
                   ps.setObject(5, produto.getGrupoProduto().getIdGrupoProduto());
               ps.setObject(6, produto.getIdProduto());
               int regAlterados = ps.executeUpdate();
               return (regAlterados == 1);
           } catch (SQLException ex) {
               throw  new RuntimeException("Não Foi possivel Atualizar Este produto");
           }
    }
    
}