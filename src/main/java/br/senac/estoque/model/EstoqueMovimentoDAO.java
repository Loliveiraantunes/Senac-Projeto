/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.estoque.model;

import Componente.Conexao;
import Componente.interfaces.BaseDAO;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.Mercadoria;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.Produto;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.TipoProduto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author loliv
 */
public class EstoqueMovimentoDAO implements BaseDAO<EstoqueMovimento, Long>{
    
    
        private Connection con = Conexao.getInstance().getConnection();
    
        public static void main(String[] args) {
            
            EstoqueMovimentoDAO emDao = new EstoqueMovimentoDAO();
           
            
            Mercadoria md = new Mercadoria();
            md.setIdProduto(4L);
           
             EstoqueMovimento em = new EstoqueMovimento();
            em.setIdMovtoEstoque(10l);
            em.setQuantidade(12D);
            em.setProduto(md);
            em.setObservacoes("Obs Att");
            em.setIdUsuario(6);
            em.setDataMovto(new Date());
            em.setTipoMovto(TipoMovimentoEstoque.ENTRADA);
            
            //emDao.inserir(em);
            //emDao.excluir(em.getIdMovtoEstoque());
          /*  for(EstoqueMovimento esm : emDao.listarPorProduto(em.getProduto().getIdProduto(), em.getDataMovto())){
                System.out.println(esm.getIdMovtoEstoque());
            }*/
         // emDao.alterar(em);
           
            
    }
        
    /**
     * A partir de um ResultSet passado como parâmetro, cujo cursor está posicionado
     * em alguma linha da tabela estoquemovto, faça o DataBinding dos dados do ResultSet 
     * para um objeto do tipo EstoqueMovimento.
     * @param rs
     */
    private EstoqueMovimento getEstoqueMovimento(ResultSet rs){
            try {
                EstoqueMovimento estoque = new EstoqueMovimento();
                
                Produto prod = new Mercadoria();
                prod.setIdProduto(rs.getLong("idProduto"));
                
                estoque.setIdMovtoEstoque(rs.getLong("idMovtoEstoque"));
                estoque.setDataMovto(rs.getDate("dataMovto"));
                estoque.setIdUsuario(rs.getInt("idUsuario"));
                estoque.setProduto(prod);
                estoque.setQuantidade(rs.getDouble("quantidade"));
                estoque.setObservacoes(rs.getString("observacoes"));
                return estoque;
            
            } catch (SQLException ex) {
                throw  new RuntimeException("Erro de conversão");
            }
        }
    

    /**
     * A partir do idMovtoEstoque, utilizando Statement ou PreparedStatement, retorne
     * o objeto do tipo EstoqueMovimento. Utilize o método getEstoqueMovimento para
     * fazer o DataBinding.
     * @param idMovtoEstoque
     * @return 
     */
    @Override
    public EstoqueMovimento getPorId(Long idMovtoEstoque) {
            try {
                String sql = "SELECT * FROM estoquemovto WHERE idMovtoEstoque = "+idMovtoEstoque;
                Statement stm = con.createStatement();
               ResultSet rs = stm.executeQuery(sql);
               if(!rs.next()){
                   throw new RuntimeException("Deu Pau, não conseguiu Retornar nada");
               }
               return getEstoqueMovimento(rs);
            } catch (SQLException ex) {
                    throw  new RuntimeException("Nenhum campo foi encontrado");
                }
    }
    /**
     * Utilizando Statement ou PreparedStatement atualize o Saldo de Estoque.
     * Se o produto não existir na tabela estoquesaldo, é necessário incluir um registro na
     * tabela estoquesaldo desse mesmo produto. Se o produto existir na tabela estoquesaldo, é necessário 
     * apenas somar a quantidade (se o parâmetro "quantidade" tiver valor positivo, vai aumentar a quantidade no estoque,
     * se for negativo, vai diminuir a quantidade no estoque).
     * @param idMovtoEstoque 
     */
    private void atualizarSaldoEstoque(Long idProduto, Double quantidade) {
            try {
                
               double valorAtual = 0D;
               
                String sqlSelect = "SELECT * FROM estoquesaldo WHERE idProduto= "+idProduto;
                Statement stm = con.createStatement();
                ResultSet rsSelect = stm.executeQuery(sqlSelect);
                
               if(rsSelect.next()){
               valorAtual=rsSelect.getDouble("saldo");
                String sql = "UPDATE estoquesaldo SET saldo = ? WHERE idProduto = ?";
                PreparedStatement pstm = con.prepareStatement(sql);
               
                pstm.setDouble(1, (valorAtual+quantidade));
                pstm.setLong(2, idProduto);
                pstm.executeUpdate();

               
               }else{
                      String sql = "INSERT INTO estoquesaldo (idProduto,saldo) values(?,?) ";
                PreparedStatement pstm = con.prepareStatement(sql);
                pstm.setDouble(2, (valorAtual+quantidade));
                pstm.setLong(1, idProduto);
                pstm.executeUpdate();
                   
               }
                               
            } catch (SQLException ex) {
                Logger.getLogger(EstoqueMovimentoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


    /**
     * Utilize PreparedStatement. 
     * Após inserir (INSERT) um registro na tabela estoquemovto, atualize também a tabela
     * estoquesaldo através do método atualizarSaldoEstoque.    
     * Utilize transação, pois iremos atualizar dois registros de tabelas diferentes, 
     * caso ocorra qualquer exceção, faça o rollback.
     * Todos os campos são obrigatórios, com exceção dos campos idUsuario e observacoes que podem ou não ser nulos (null).
     * @param movtoEstoque
     */
    @Override
    public Long inserir(EstoqueMovimento movtoEstoque) {
            try {
                String sql = "INSERT INTO estoquemovto (quantidade,tipoMovto,dataMovto,idProduto,idUsuario,Observacoes) VALUES(?,?,?,?,?,?)";
                PreparedStatement pstm = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
                pstm.setDouble(1, movtoEstoque.getQuantidade());
                pstm.setString(2, movtoEstoque.getTipoMovto().getCodigo().toString());
                pstm.setTimestamp(3, new java.sql.Timestamp(movtoEstoque.getDataMovto().getTime()));
                pstm.setLong(4, movtoEstoque.getProduto().getIdProduto());
                pstm.setLong(5, movtoEstoque.getIdUsuario());
                pstm.setString(6, movtoEstoque.getObservacoes());
                pstm.executeUpdate();
                ResultSet rs = pstm.getGeneratedKeys();
                
                if(!rs.next()){
                    throw new RuntimeException("Erro ao Inserir os valores, verifique os campos");
                }
                atualizarSaldoEstoque(movtoEstoque.getProduto().getIdProduto(), movtoEstoque.getQuantidade());
                return rs.getLong(1);
            } catch (SQLException ex) {
                throw new RuntimeException(ex.getMessage(),ex);
            }
    }



    /**
     * Utilize PreparedStatement. 
     * Será necessário buscar a quantidade do movimento que está sendo excluído,
     * depois atualizar o estoquesaldo através do método atualizarSaldoEstoque, por último, 
     * faça a exclusão (DELETE) do registro em estoquemovto conforme idMovtoEstoque passado.
     * Transacione essa operação, caso ocorra alguma exceção, faça rollback.
     * @param idMovtoEstoque
     * @return
     */
    @Override
    public boolean excluir(Long idMovtoEstoque) {
            try {
                String sqlSelect = "SELECT * FROM estoquemovto WHERE idMovtoEstoque = "+idMovtoEstoque;
                 Statement stmS = con.createStatement();
                 ResultSet resset = stmS.executeQuery(sqlSelect);
                 double quantidade = 0D;
                 Long idProduto = -1L ;
                 if(resset.next()){
                     quantidade = resset.getDouble("quantidade");
                     idProduto = resset.getLong("idProduto");
                 }
                 atualizarSaldoEstoque(idProduto, quantidade);
                
                 String sql = "Delete FROM estoquemovto WHERE idMovtoEstoque = "+idMovtoEstoque;
                
                 PreparedStatement stm = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
               
                stm.executeUpdate();
                
                ResultSet rs = stm.getGeneratedKeys();
                if(rs.next()) return  true;
           
            } catch (SQLException ex) {
                    return false;
            }
        return  false;
    }



    /**
     * Utilize Statement. 
     * Será necessário buscar a quantidade do movimento que está sendo alterado,
     * caso a quantidade que está no banco de dados for diferente da quantidade
     * passada no atributo movtoEstoque.getQuantidade(), é necessário atualizar a tabela
     * estoquesaldo através do método atualizarSaldoEstoque.
     * Após atualização do saldo, alterar (UPDATE) o registro na tabela estoquemovto. 
     * Utilize transação, se qualquer exceção ocorrer, faça o rollback.
	 * Todos os campos são obrigatórios, com exceção dos campos idUsuario e observacoes que podem ou não ser nulos (null).	
     * @param movtoEstoque
     */
    @Override
    public boolean alterar(EstoqueMovimento movtoEstoque)  {
            try {
                String Select = "SELECT * FROM estoquemovto WHERE idmovtoestoque ="+movtoEstoque.getIdMovtoEstoque();
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery(Select);
                if(rs.next()){
                     if(rs.getDouble("quantidade") != movtoEstoque.getQuantidade()){
                         atualizarSaldoEstoque(movtoEstoque.getProduto().getIdProduto(), movtoEstoque.getQuantidade());
                     }   
                }
                String sql = "UPDATE estoquemovto SET quantidade = ? , tipoMovto = ? ,dataMovto = ? , idProduto = ?, idUsuario = ?, observacoes = ? WHERE idMovtoEstoque = ?";
                PreparedStatement pstm = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
                pstm.setDouble(1, movtoEstoque.getQuantidade());
                pstm.setString(2, movtoEstoque.getTipoMovto().getCodigo().toString());
                pstm.setTimestamp(3,new java.sql.Timestamp(movtoEstoque.getDataMovto().getTime()));
                pstm.setLong(4, movtoEstoque.getProduto().getIdProduto());
                pstm.setLong(5, movtoEstoque.getIdUsuario());
                pstm.setString(6, movtoEstoque.getObservacoes());
                pstm.setLong(7, movtoEstoque.getIdMovtoEstoque());
                pstm.executeUpdate();
                ResultSet rst = pstm.getGeneratedKeys();
                if(rst.next()){
                    return true;
                } 
                return false;
            } catch (SQLException ex) {
                throw new RuntimeException("Deu Ruim",ex);
            }
    }
    

    /**
     * Utilizando Statement ou PreparedStatement, a partir do idProduto informado 
     * e dataInicioMovto (considere como DATE apenas), retorne a lista de objetos do tipo EstoqueMovimento.
     * Utilize a função getEstoqueMovimento para fazer o DataBinding dos dados
     * de cada uma das linhas do ResultSet para o objeto EstoqueMovimento.
     * Retorne apenas os registro cujo dataMovto seja maior ou igual a dataInicioMovto.
     * @param idProduto
     */
    public List<EstoqueMovimento> listarPorProduto(Long idProduto, Date dataInicioMovto){
            try {
                ArrayList<EstoqueMovimento> estoque = new ArrayList<>();
                String sql = "SELECT * FROM estoquemovto WHERE idProduto = ? AND dataMovto >= ? ";
                PreparedStatement stm = con.prepareStatement(sql);
                stm.setLong(1, idProduto);
                stm.setDate(2, new java.sql.Date(dataInicioMovto.getTime()));
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    estoque.add(getEstoqueMovimento(rs));
                }
                return estoque;
            } catch (SQLException ex) {
                throw new RuntimeException("Deu Ruim isso aq, vai da Não.");
            }
    }
    
}