/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto;

import br.senac.senac_atividades_obrigatorias.grupoproduto.model.GrupoProduto;
import br.senac.senac_atividades_obrigatorias.grupoproduto.modulo.GrupoProdutoDAO;
import java.sql.SQLException;

/**
 *
 * @author loliv
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        GrupoProdutoDAO gp = new GrupoProdutoDAO();
        for(GrupoProduto prod : gp.listarTodos()) {
            System.out.println(prod.getNomeGrupoProduto());
        }
        
        
    }
}
