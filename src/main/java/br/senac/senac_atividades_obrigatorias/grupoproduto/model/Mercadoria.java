/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto.model;

import java.util.Date;

/**
 *
 * @author loliv
 */
public class Mercadoria extends Produto{

    private long idMercadoria;
    private byte[] imagem;

    private Long idProduto;
    private String nomeProduto;
    private TipoProduto tipoProduto;
    private String descricao;
    private Date dataCriacao;
    private Date dataAlteracao;
    private float perciCMS;
    private GrupoProduto grupoProduto;
    
    public long getIdMercadoria() {
        return idMercadoria;
    }

    public void setIdMercadoria(long idMercadoria) {
        this.idMercadoria = idMercadoria;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    @Override
    public Long getIdProduto() {
        return idProduto;
    }

    @Override
    public void setIdProduto(Long idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public String getNomeProduto() {
        return nomeProduto;
    }

    @Override
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    @Override
    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    @Override
    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    @Override
    public String getDescricao() {
        return descricao;
    }

    @Override
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public Date getDataCriacao() {
        return dataCriacao;
    }

    @Override
    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    @Override
    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    @Override
    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    @Override
    public float getPerciCMS() {
        return perciCMS;
    }

    @Override
    public void setPerciCMS(float perciCMS) {
        this.perciCMS = perciCMS;
    }

    @Override
    public GrupoProduto getGrupoProduto() {
        return grupoProduto;
    }

    @Override
    public void setGrupoProduto(GrupoProduto grupoProduto) {
        this.grupoProduto = grupoProduto;
    }

    @Override
    public float getTotalPercImposto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
   
    
    
}