/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto.model;

import java.util.Date;


/**
 *
 * @author loliv
 */
public class GrupoProduto {
    
    private Integer idGrupoProduto;
    private String nomeGrupoProduto;
    private TipoProduto tipoProduto;
    private Date dataExclusao;
    private Date dataInclusao;
    private Double percDesconto;
    
    
    public GrupoProduto(){
        
    }
    
    public GrupoProduto(Integer id,String nomeProduto, TipoProduto tipoProduto){
       idGrupoProduto = id;
        nomeGrupoProduto = nomeProduto;
        this.tipoProduto = tipoProduto;
    }

    public Integer getIdGrupoProduto() {
        return idGrupoProduto;
    }

    public void setIdGrupoProduto(Integer idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
    }

    public String getNomeGrupoProduto() {
        return nomeGrupoProduto;
    }

    public void setNomeGrupoProduto(String nomeGrupoProduto) {
        this.nomeGrupoProduto = nomeGrupoProduto;
    }

    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Double getPercDesconto() {
        return percDesconto;
    }

    public void setPercDesconto(Double percDesconto) {
        this.percDesconto = percDesconto;
    }
    
    
  
    

    
}
