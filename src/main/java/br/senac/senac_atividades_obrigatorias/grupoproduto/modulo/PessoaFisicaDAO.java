/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;

import Componente.Conexao;
import Componente.interfaces.BaseDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;



/**
 *
 * @author Gerson
 */
public class PessoaFisicaDAO implements BaseDAO<PessoaFisica, Long>{

    
    private Conexao conexao = Conexao.getInstance();
    
    public static void main(String[] args) {
        PessoaFisica pFisica = new PessoaFisica();
        pFisica.setNome("Maria da Silva");
        pFisica.setCpf(Long.toString(10000000000L + (System.currentTimeMillis() % 10000000000L)));
        pFisica.setDtNascimento(new GregorianCalendar(1975, Calendar.APRIL, 22).getTime());
        pFisica.setSexo(Sexo.FEMININO);
        PessoaFisicaDAO dao = new PessoaFisicaDAO();
        dao.inserir(pFisica);
    }
    
    @Override
    public PessoaFisica getPorId(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean excluir(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean alterar(PessoaFisica objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long inserir(PessoaFisica pessoaFis) {
        
        String sqlPessoa = "insert into pessoa\n" +
                            "(nomePessoa, tipo, telefone, email,\n" +
                            "idEndereco,dtcadastro) VALUES\n" +
                            "(?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement ps = conexao.getConnection().prepareStatement(sqlPessoa,
                                  PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pessoaFis.getNome());
            ps.setString(2, "F");
            ps.setObject(3, pessoaFis.getTelefone());
            ps.setObject(4, pessoaFis.getEmail());
            ps.setObject(5, null);
            ps.setTimestamp(6, new java.sql.Timestamp(System.currentTimeMillis()));
            
            ps.executeUpdate();
            
            ResultSet rsChave = ps.getGeneratedKeys();
            Long idChave;
            if(rsChave.next()){
                idChave = rsChave.getLong(1);
            }else {
                throw new RuntimeException("Erro ao tentar inserir pessoa.");
            }
            
            //continue aqui fazendo a inserção da pessoafisica
            
            return idChave;
            
        }catch(Exception e){
            throw new RuntimeException("Erro ao ins"
                    + "erir pessoa física: " +
                                        e.getMessage(), e);
        }
        
    }
	
    /**
    * Se o nome for nulo ou tiver menos de três caracteres será gerada uma exceção para evitar o retorno de muitos registros.
    */
    public List<PessoaFisica> getPorNome(String nome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }	
    
}
