/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;

import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author loliv
 */
public class Usuario {
  private Long idUsuario;
    
    private String login;
    
    private String senha;
    
    private Date dataExpiracao;
    
    private GrupoUsuario grupoUsuario;

    public Usuario() {
    }

    public Usuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuario(Long idUsuario, String login, String senha, Date dataExpiracao, Integer idGrupoUsuario) throws SQLException {
        this.idUsuario = idUsuario;
        this.login = login;
        this.senha = senha;
        this.dataExpiracao = dataExpiracao;
        setIdGrupoUsuario(idGrupoUsuario);
    }
    
    public Usuario(String login, String senha, Date dataExpiracao, Integer idGrupoUsuario) throws SQLException {
        this.login = login;
        this.senha = senha;
        this.dataExpiracao = dataExpiracao;
        setIdGrupoUsuario(idGrupoUsuario);
    }    

        
    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(Date dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

 /*   public Integer getIdGrupoUsuario() {
        return (grupoUsuario == null ? null : grupoUsuario.getIdGrupoUsuario());
    }
*/
    public void setIdGrupoUsuario(Integer idGrupoUsuario) throws SQLException {
        if(idGrupoUsuario == null || idGrupoUsuario <= 0){
            this.grupoUsuario = null;
        }else {
            GrupoUsuarioDAO dao = new GrupoUsuarioDAO();
                
            //grupoUsuario = dao.getUsuarioPorId(idGrupoUsuario);
        //    this.grupoUsuario = dao.getUsuarioPorId(idGrupoUsuario);
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.idUsuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.idUsuario, other.idUsuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", login=" + login + ", senha=" + senha + ", dataExpiracao=" + dataExpiracao + ", grupoUsuario=" + grupoUsuario + '}';
    }
    
    public GrupoUsuario getGrupoUsuario() {
        return grupoUsuario;
    }

    public void setGrupoUsuario(GrupoUsuario grupoUsuario) {
        this.grupoUsuario = grupoUsuario;
    }

    Object getIdGrupoUsuario() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
       
}
