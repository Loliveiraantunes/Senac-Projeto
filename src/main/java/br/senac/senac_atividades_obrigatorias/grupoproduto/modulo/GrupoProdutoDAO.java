/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;

import Componente.Conexao;
import Componente.interfaces.BaseDAO;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.GrupoProduto;
import br.senac.senac_atividades_obrigatorias.grupoproduto.model.TipoProduto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author loliv
 */
public class GrupoProdutoDAO implements BaseDAO< GrupoProduto, Integer> {
    public Conexao conexao = Conexao.getInstance();
    @Override
    public GrupoProduto getPorId(Integer id) {
        try {
            Statement stm = conexao.getConnection().createStatement();
            String sql = "Select * from grupoproduto "
                    + " where idgrupoproduto = " + id;
            ResultSet rs = stm.executeQuery(sql);
            if (rs.next() == false) {
                throw new RuntimeException("Registro não encontrado!");
            }
            return getGrupoProduto(rs);
        } catch (SQLException ex) {
            throw new RuntimeException("Ocorreu um erro inesperado ao consultar"
                    + " dados em grupo produto, consulte o suporte:" + ex.getMessage(), ex);
        }
    }

    private GrupoProduto getGrupoProduto(ResultSet rs) {
        try {
            GrupoProduto gp = new GrupoProduto();
            
            if( rs.getString("idgrupoproduto") != null){
                gp.setIdGrupoProduto(rs.getInt("idgrupoproduto"));
            }
            gp.setNomeGrupoProduto(rs.getString("nomeGrupoProduto"));
            gp.setDataExclusao(rs.getDate("dataexclusao")); //
            gp.setDataInclusao(rs.getTimestamp("datainclusao"));
            switch (rs.getInt("tipo")) {
                case 1:
                    gp.setTipoProduto(TipoProduto.MERCADORIA);
                    break;
                case 2:
                    gp.setTipoProduto(TipoProduto.SERVICO);
                    break;
                case 3:
                    gp.setTipoProduto(TipoProduto.MATERIA_PRIMA);
                    break;
            }
            return gp;
        } catch (SQLException ex) {
            Logger.getLogger(GrupoProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro inesperado: " + ex.getMessage(), ex);
        }
    }

    @Override
    public boolean excluir(Integer id) {
        String sqlExclusao = "DELETE FROM grupoproduto WHERE idgrupoproduto = " + id;
        try {
            Statement stm = conexao.getConnection().createStatement();
            int regAlterados = stm.executeUpdate(sqlExclusao);
            return (regAlterados == 1);
        } catch (SQLException e) {
            String sqlAtualizaData = "UPDATE grupoproduto set dataexclusao = CURDATE() "
                    + "WHERE idgrupoproduto = " + id;
            Statement stm;
            int regAlterados = 0;
            try {
                stm = conexao.getConnection().createStatement();
                regAlterados = stm.executeUpdate(sqlAtualizaData);
            } catch (SQLException ex) {
                Logger.getLogger(GrupoProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            return (regAlterados == 1);
        }
    }

    @Override
    public boolean alterar(GrupoProduto grupoProduto) {
        String sqlAlterar = "UPDATE grupoproduto SET ";
        sqlAlterar += " nomegrupoproduto = '" + grupoProduto.getNomeGrupoProduto() + "', \n";
        sqlAlterar += "	tipo = " + grupoProduto.getTipoProduto().getId() + ", \n";
        sqlAlterar += " percdesconto = " + grupoProduto.getPercDesconto() + " \n";
        sqlAlterar += "WHERE idgrupoproduto = " + grupoProduto.getIdGrupoProduto();
        Statement stm;
        try {
            stm = conexao.getConnection().createStatement();
            int regAlterados = stm.executeUpdate(sqlAlterar);
            return (regAlterados == 1);
        } catch (SQLException ex) {
            Logger.getLogger(GrupoProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Integer inserir(GrupoProduto grupoProduto) {
        //if(grupoProduto.getDataInclusao() == null)
        grupoProduto.setDataInclusao(new Date());

        Integer pk;
        String sqlInsert = "INSERT INTO GRUPOPRODUTO "
                + "(NOMEGRUPOPRODUTO, TIPO, DATAINCLUSAO, PERCDESCONTO) "
                + "VALUES (";
        sqlInsert += "'" + grupoProduto.getNomeGrupoProduto() + "', ";
        if (grupoProduto.getTipoProduto() == null) {
            throw new RuntimeException("Tipo grupo não pode ser nulo.");
        } else {
            sqlInsert += grupoProduto.getTipoProduto().getId() + ",";
        }
        sqlInsert += UtilSQL.getDataTempoToSQL(grupoProduto.getDataInclusao()) + ",";
        sqlInsert += grupoProduto.getPercDesconto() + ")";
        System.out.println(sqlInsert);

        Statement stm;
        try {
            stm = conexao.getConnection().createStatement();

            int regCriados = stm.executeUpdate(sqlInsert,
                    Statement.RETURN_GENERATED_KEYS);
            ResultSet rsPK = stm.getGeneratedKeys();   //Obter PK gerada
            if (rsPK.next()) {
                pk = rsPK.getInt(1);
                return pk;
            }
        } catch (SQLException ex) {
            Logger.getLogger(GrupoProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        throw new RuntimeException("Erro inesperado ao incluir grupo produto!");
    }

    public boolean equals(GrupoProduto produto) {
        return true;
    }

    public List<GrupoProduto> listarPorNome(String nome) throws SQLException {

         if(nome == null && nome.trim().equals("")){
            return listarTodos();
        }
        List<GrupoProduto> lista = new ArrayList<>();
        String sql = "SELECT * FROM grupoproduto"
                + "WHERE UPPER(nomegrupoproduto) LIKE UPPER('%"+ nome +"%')";
        Statement stm = conexao.getConnection().createStatement();
        ResultSet rs = stm.executeQuery(sql);
        GrupoProduto grupProd;
        while(rs.next()){
            grupProd = getGrupoProduto(rs);
            lista.add(grupProd);
        }
        return lista;
    }

    public List<GrupoProduto> ordernarPorNome(boolean crescente) throws SQLException{
          List<GrupoProduto> lista = listarTodos();
        String sql = "SELECT * FROM grupoproduto "
                + "ORDER BY nomegrupoproduto ";
        if(crescente)
            sql += "ASC";
        else
            sql += "DESC";
        
        Statement stm = conexao.getConnection().createStatement();
        ResultSet rs = stm.executeQuery(sql);
        GrupoProduto grupProd;
        while(rs.next()){
            grupProd = getGrupoProduto(rs);
            lista.add(grupProd);
        }
        return lista;
    }
    
    public List<GrupoProduto> listarPorTipo(TipoProduto tipoProduto) throws SQLException {

        List<GrupoProduto> lista = listarTodos();
        String sql = "SELECT * FROM grupoproduto "
                + "WHERE tipo = ? ORDER BY nomeGrupoProduto";
        
        PreparedStatement stm = (PreparedStatement) conexao.getConnection().createStatement();
        stm.setInt(1,tipoProduto.getId());
        ResultSet rs = stm.executeQuery(sql);
        GrupoProduto grupProd;
        while(rs.next()){
            grupProd = getGrupoProduto(rs);
            lista.add(grupProd);
        }
        return lista;
    }

    public List<GrupoProduto> listarTodos() throws SQLException {
    List<GrupoProduto> lista = new ArrayList<>();
    
        String sql = "SELECT * FROM grupoproduto";
    
        Statement stm = conexao.getConnection().createStatement();
        ResultSet rs = stm.executeQuery(sql);
        
        GrupoProduto grupProd;
        
        while(rs.next()){
            grupProd = getGrupoProduto(rs);
            lista.add(grupProd);
        }
        return lista;
    }
}
