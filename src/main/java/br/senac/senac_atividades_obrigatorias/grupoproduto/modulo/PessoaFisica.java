package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;

import java.util.Date;

public class PessoaFisica extends Pessoa {

    private String cpf;
    private Date dtNascimento;
    private String rg;
    private Sexo sexo;
    private Boolean pcd;
    private TipoDeficiencia tipoDeficiencia;
    private Double renda;

    public PessoaFisica(String cpf, Date dtNascimento, String rg) {
        this.cpf = cpf;
        this.dtNascimento = dtNascimento;
        this.rg = rg;
    }

    public PessoaFisica(Long id, String nome, String tipo) {
        super(id, nome, tipo);
    }
    
    

    public PessoaFisica(Long id, String nome, Date dtNascimento) {
        super(id, nome);
        this.dtNascimento = dtNascimento;
    }
    
    public PessoaFisica() {
    }


    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    @Override
    public String getDocumento() {
        return cpf;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Boolean getPcd() {
        return pcd;
    }

    public void setPcd(Boolean pcd) {
        this.pcd = pcd;
    }

    public TipoDeficiencia getTipoDeficiencia() {
        return tipoDeficiencia;
    }

    public void setTipoDeficiencia(TipoDeficiencia tipoDeficiencia) {
        this.tipoDeficiencia = tipoDeficiencia;
    }

    public Double getRenda() {
        return renda;
    }

    public void setRenda(Double renda) {
        this.renda = renda;
    }
    
    

    @Override
    public String toString() {
        return super.toString() + " PessoaFisica{" + "cpf=" + cpf + ", dtNascimento=" + dtNascimento + ", rg=" + rg + ", sexo=" + sexo + '}';
    }

    
    
  
    

}
