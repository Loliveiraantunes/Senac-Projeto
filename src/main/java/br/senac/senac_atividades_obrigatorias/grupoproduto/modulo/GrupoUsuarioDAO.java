/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;

import Componente.Conexao;
import java.security.InvalidParameterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author loliv
 */
class GrupoUsuarioDAO {
    
    
    private Conexao conexao = Conexao.getInstance();
 
       public static void main(String[] args) throws SQLException {
        GrupoUsuarioDAO dao = new GrupoUsuarioDAO();
        Usuario usuarioStatement = new Usuario("jose", "j001", new Date(), 1);
        Integer idStament = dao.incluirStatement(usuarioStatement);
        System.out.println("id Statement: " + idStament);

        usuarioStatement = null;
        //buscar os usuários do banco de dados novamente para alteração
        usuarioStatement = dao.getUsuarioPorId(idStament);

        System.out.println("Usuário stament: " + usuarioStatement.getLogin());
        usuarioStatement.setLogin("jose 1");
        dao.alterarStatement(usuarioStatement);
        usuarioStatement = dao.getUsuarioPorId(idStament);
        System.out.println("Usuário stament após alteração: " + usuarioStatement);

        dao.excluirStatement(usuarioStatement);
    }

    public Usuario getUsuarioPorId(Integer idUsuario) throws SQLException {
        String sql = "select * from usuario "
                + "where idusuario = " + idUsuario;
        //Primeiro preciso da conexão
        
        //Agora preciso da classe que executa comandos, PreparedStatement
        Statement stm = conexao.getConnection().createStatement();
        ResultSet rs = stm.executeQuery(sql);
        //Vai para posição 1, se não existir usuário, next retorna false
        if (!rs.next()) {
            throw new InvalidParameterException("Usuário não existe: " + idUsuario + ". Consulte o suporte!");
        }

        return getUsuario(rs);
    }

    public Integer incluirStatement(Usuario usuario) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        /*Concatenar strings */
        String sql = "INSERT INTO usuario\n"
                + //O /n faz a fução de um espaço em branco entre os comandos
                "(login,\n"
                + "senha,\n"
                + "dataExpiracao,\n"
                + "idGrupoUsuario)\n"
                + "VALUES\n"
                + "('" + usuario.getLogin() + "',\n"
                + "'" + usuario.getSenha() + "',\n" //varchar, na linguagem SQL tem que ir entre aspas simples
                + "" + (usuario.getDataExpiracao() == null ? "null" : "{d '" + sdf.format(usuario.getDataExpiracao()) + "'}") + ",\n"
                + "" + (usuario.getIdGrupoUsuario() == null ? "null" : usuario.getIdGrupoUsuario().toString()) + ")";
        //Valida se a data de expiração não é nula antes de inserir
        //O formato da data é 'yyyy-MM-dd'
        //Primeiro preciso da conexão
        //Agora preciso da classe que executa comandos, Statement
        Statement stm = conexao.getConnection().createStatement();
        //Executo o comando sql de atualização
        stm.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
        ResultSet rsChave = stm.getGeneratedKeys();
        if (rsChave.next()) {
            return rsChave.getInt(1);
        }
        throw new InvalidParameterException("Erro na inserção do usuário: " + usuario.getLogin() + ".");
    }

    public void alterarStatement(Usuario usuario) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        /*Concatenar strings */
        String sql = "UPDATE usuario "
                + "SET login = '" + usuario.getLogin() + "', \n"
                + "senha = '" + usuario.getSenha() + "',\n"
                + "dataExpiracao = " + (usuario.getDataExpiracao() == null ? "null" : "{d '" + sdf.format(usuario.getDataExpiracao()) + "'}") + ", \n"
                + "idGrupoUsuario = " + (usuario.getIdGrupoUsuario() == null ? "null" : usuario.getIdGrupoUsuario().toString()) + "\n"
                + "WHERE idUsuario = " + usuario.getIdUsuario();
        //Valida se a data de expiração não é nula antes de inserir
        //O formato da data é 'yyyy-MM-dd'
        //Primeiro preciso da conexão
        //Agora preciso da classe que executa comandos, Statement
        Statement stm = conexao.getConnection().createStatement();
        //Executo o comando sql de atualização
        int registrosAtualizados = stm.executeUpdate(sql);
        if (registrosAtualizados == 0) {
            throw new InvalidParameterException("Erro ao atualizar o usuário: " + usuario.getLogin() + ". Consulte o suporte!");
        }
    }

    public void excluirStatement(Usuario usuario) throws SQLException {
        String sql = "DELETE FROM usuario Where idUsuario = " + usuario.getIdUsuario();
        //Primeiro preciso da conexão
        //Agora preciso da classe que executa comandos, Statement
        Statement stm = conexao.getConnection().createStatement();
        //Executo o comando sql de atualização
        int registrosAtualizados = stm.executeUpdate(sql);
        if (registrosAtualizados == 0) {
            throw new InvalidParameterException("Erro ao excluir o usuário: " + usuario.getLogin() + ". Consulte o suporte!");
        }
    }

    public ArrayList<Usuario> getUsuarioPorNome(
            String paramLogin) throws SQLException {
        String sql = "select * from usuario "
                + " where UPPER(login) like "
                + " UPPER('%" + paramLogin + "%') "
                + "Order by login ";
        //Primeiro preciso da conexão

        //Agora preciso da classe que executa comandos, Statement
        Statement stm = conexao.getConnection().createStatement();
        ResultSet rs = stm.executeQuery(sql);
        ArrayList<Usuario> lista = new ArrayList<>();

        while (rs.next()) {
			//Adiciona o objeto do tipo Usuario obtido do método getUsuario
            lista.add(getUsuario(rs));
        }
		
        return lista;
    }

    private Usuario getUsuario(ResultSet rs) throws SQLException {
        Usuario usuario = new Usuario();
        usuario.setDataExpiracao(rs.getDate("dataExpiracao"));
        usuario.setIdGrupoUsuario(rs.getInt("idGrupoUsuario"));
        usuario.setIdUsuario(rs.getLong("idUsuario"));
        usuario.setLogin(rs.getString("login"));
        usuario.setSenha(rs.getString("senha"));
        return usuario;
    }
}
