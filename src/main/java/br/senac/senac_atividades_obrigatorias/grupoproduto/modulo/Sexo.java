
package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;


public enum Sexo {
    MASCULINO, FEMININO;
}
