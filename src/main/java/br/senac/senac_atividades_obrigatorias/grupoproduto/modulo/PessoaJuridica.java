package br.senac.senac_atividades_obrigatorias.grupoproduto.modulo;

public class PessoaJuridica extends Pessoa {
  
    private String cnpj;
    private String atividade;

    public PessoaJuridica(Long id, String nome, String tipo) {
        super(id, nome, tipo);
    }

    public PessoaJuridica() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public PessoaJuridica(String cnpj, String atividade) {
        this.cnpj = cnpj;
        this.atividade = atividade;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }


    @Override
    public String getDocumento() {
        return cnpj;
    }
    
    
}
